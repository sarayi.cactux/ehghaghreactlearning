import React from "react";
import Cookies from "js-cookie";

import AuthApi from "./components/store/AuthApi";
import Loading from "./components/layout/Loading";
import GetUserInf from "./components/api/GetUserInf";
import AppRoutes from "./components/AppRoutes";

function App() {
  const [checked, setChecked] = React.useState(false);
  const Auth = React.useContext(AuthApi);
  Loading();
  React.useEffect(() => {
    const readcookie = async () => {
      const token = Cookies.get("ehghaghUser");
      if (token) {
        const result = await GetUserInf(token);
        if (result) {
          Cookies.set("ehghaghUser", result.data.token, { expires: 7 });
          Auth.setAuth(true);
          Auth.setUserInfo(result.data);
        }
        
      }
      setChecked(true);
    };
    readcookie();
    Auth.setLoading(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [Auth.auth]);

  return <div>{checked ? <AppRoutes /> : null}</div>;
}

export default App;
