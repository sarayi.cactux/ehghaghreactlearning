import AuthApi from "../store/AuthApi";
import React from "react";
import Cookies from "js-cookie";

import validateMobile from "../common/validateMobile";
import SingIn from "../api/SingIn";
import LoginUser from "../api/LoginUser";

const Login = () => {
  const mystyle = {
    cursor: "pointer",
  }
  const [checkMobile, setChekcMobile] = React.useState(false);
  const [mobileNumber, setMobileNumber] = React.useState("");
  const [token, setToken] = React.useState("");
  const [sendMobileBtn, setSendMobileBtn] = React.useState("ارسال پیامک ورود");
  const mobileNumberRef = React.useRef();
  const codeRef = React.useRef();
  const codeBtnRef = React.useRef();
  const Auth = React.useContext(AuthApi);
  const loginHandler = async (event) => {
    event.preventDefault();
    codeBtnRef.current.value = "... در حال ارسال";
    const code = codeRef.current.value;
    if (code.length === 4) {
      const result = await LoginUser(token, parseInt(code));

      if (result) {
        Cookies.set("ehghaghUser", result.data.token, { expires: 7 });
        Auth.setAuth(true);
        Auth.setUserInfo(result.data);
      } else {
        codeBtnRef.current.value = "کد وارد شده معتبر نیست";
        setTimeout(() => {
          codeBtnRef.current.value = "ورود";
        }, 1500);
      }
    } else {
      codeBtnRef.current.value = "کد وارد شده معتبر نیست";
      setTimeout(() => {
        codeBtnRef.current.value = "ورود";
      }, 1500);
    }
  };

  const sendCode = async (mobileNumber) => {
    const singIntoken = await SingIn(mobileNumber);
    setToken(singIntoken.data);
  };

  const sendMobile = (event) => {
    event.preventDefault();
    const mobileNumber = mobileNumberRef.current.value;
    if (validateMobile(mobileNumber)) {
      setMobileNumber(mobileNumber);
      setChekcMobile(true);
      sendCode(mobileNumber);
    } else {
      setSendMobileBtn("شماره تلفن همراه معتبر نیست");
      setTimeout(() => {
        setSendMobileBtn("ارسال پیامک ورود");
      }, 1500);
    }
  };
  const resetForm = () => {
    setChekcMobile(false);
  };
  const resendCode = () => {
    sendCode(mobileNumber);
  };
  const SendCodeForm = () => {
    return (
      <form className="login-form" onSubmit={loginHandler}>
        <h4 className="text-start my-4 font-size18"> ورود به حساب احقاق </h4>
        <span className="d-flex justify-content-end mb-3 font-size14 color-login">
          پیامک کد تایید به شماره شما ارسال شد
        </span>
        <div className="flex-row">
          <input
            ref={codeRef}
            id="username"
            className="lf--input"
            placeholder="کد تایید وارد کنید"
            type="number"
          />
        </div>

        <input
          className="lf--submit"
          type="submit"
          ref={codeBtnRef}
          value="ورود"
        />
        <span className="mt-3 d-flex justify-content-center">
          <span
          style={mystyle}
            className="lf--forgot text-decoration-none"
            onClick={resendCode}
          >
            ارسال مجدد پیامک |&nbsp;
          </span>
          <span style={{cursor: "pointer"}} className="lf--forgot text-decoration-none" onClick={resetForm}>
            برگشت{" "}
          </span>
        </span>
      </form>
    );
  };
  const Mobileform = (props) => {
    return (
      <form className="login-form" onSubmit={sendMobile}>
        <h4 className="text-start my-4 font-size18"> ورود به حساب احقاق </h4>
        <div className="flex-row">
          <input
            id="mobileNumber"
            defaultValue={props.mobileNumber || ""}
            ref={mobileNumberRef}
            className="lf--input"
            placeholder="شماره تلفن همراه خود را وارد کنید"
            type="number"
          />
        </div>
        <button className="lf--submit" type="submit" id="submit1">
          {sendMobileBtn}
        </button>
      </form>
    );
  };
  return (
    <div className="mt-5 pt-5">
      <div className="row mx-auto d-flex justify-content-center">
        <div className="col-12 col-md-6 mt-5 pt-5   body">
          {checkMobile ? (
            <SendCodeForm />
          ) : (
            <Mobileform mobileNumber={mobileNumber} />
          )}
        </div>{" "}
      </div>
    </div>
  );
};

export default Login;
