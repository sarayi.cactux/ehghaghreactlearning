import { Link, useNavigate } from "react-router-dom";

import React from "react";

import AuthApi from "../../store/AuthApi";
import { SuccessNotify, ErrorNotify } from "../../common/Toast";
import FileUploader from "../../layout/FileUploader";
import MainDeparment from "../../api/MainDeparment";
import SecondDeparment from "../../api/SecondDeparment";

import NewRequestApi from "../../api/NewRequest";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import VoiseRecorder from "../../layout/VoiseRecorder";

import microphone from "../../../assests/image/icons/microphone.svg";
import arrowLeft from "../../../assests/image/icons/arrow-left.svg";

function NewRequest() {
  const navigate = useNavigate();
  const submitRef = React.useRef();
  const mainDepartmentRef = React.useRef();
  const subjectRef = React.useRef();
  const secondDeparmentRef = React.useRef();
  const textRef = React.useRef();
  const Auth = React.useContext(AuthApi);
  const [mainDeparments, setMainDepartment] = React.useState([]);
  const [secondDeparments, setSecondDeparments] = React.useState([]);
  const [uploadedFiles, setUploadedFiles] = React.useState([]);
  const [havePermissions, setHavePermissions] = React.useState(false);
  const [voiseUploaded, setVoiseUploaded] = React.useState(false);
  const [voise, setVoise] = React.useState(null);
  const token = Auth.userInfo.token;

  React.useEffect(() => {
    Auth.setLoading(true);

    const findMainDepartment = async () => {
      const result = await MainDeparment();
      setMainDepartment(result.data);
      Auth.setLoading(false);
    };
    findMainDepartment();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const isLawyer = false;
  if (isLawyer) {
    navigate("/");
  }

  const checkPermissions = async () => {
    const permissions = navigator.mediaDevices.getUserMedia({ audio: true });
    permissions
      .then((stream) => {
        setHavePermissions(true);
      })
      .catch((err) => {
        setHavePermissions(false);
      });
  };
  const sendNewRequest = async (event) => {
    event.preventDefault();
    const subject = subjectRef.current.value;
    if (!subject || subject.length < 5) {
      ErrorNotify("موضوع درخواست وارد نشده");
      return;
    }
    const consultingType = "مشاوره";
    const mainDeparment = parseInt(mainDepartmentRef.current.value);
    if (mainDeparment === 0) {
      ErrorNotify("دسته بندی اول انتخاب نشده");
      return;
    }
    const secondDepartment = parseInt(secondDeparmentRef.current.value);
    if (secondDepartment === 0) {
      ErrorNotify("دسته بندی دوم انتخاب نشده");
      return;
    }
    const text = textRef.current.value;
    if (!text || text.length < 15) {
      ErrorNotify("متن درخواست وارد نشده");
      return;
    }
    if (voiseUploaded) {
      uploadedFiles.push(voise);
    }

    const medias = uploadedFiles.map((item) => {
      return {
        mediaId: item.id,
        title: subject,
      };
    });
    Auth.setLoading(true);
    const newRequestObj = {
      subject,
      consultingType,
      mainDeparment,
      secondDepartment,
      text,
      medias,
    };
    const newUserInfo = await NewRequestApi(token, newRequestObj);
    if (newUserInfo.status === 403) {
      Auth.setUserInfo(null);
      Auth.setAuth(false);
      return;
    }
    if (!newUserInfo) {
      Auth.setUserInfo(null);
      Auth.setAuth(false);
      return;
    }

    Auth.setUserInfo(newUserInfo.data);
    Auth.setLoading(false);

    SuccessNotify("درخواست با موفقیت ثبت شد");
    setTimeout(() => {
      navigate("/");
    }, 3000);
  };
  checkPermissions();
  const secondDepartmentHandler = async () => {
    const mainDepartmentId = mainDepartmentRef.current.value;
    Auth.setLoading(true);
    const result = await SecondDeparment(mainDepartmentId);
    setSecondDeparments(result.data);
    Auth.setLoading(false);
  };

  

  const voiseTag = (
    <div
      className="pointer"
      onClick={() => {
        setVoiseUploaded(true);
      }}
    >
      <img alt="" src={microphone} width="20" height="27" />
      در صورت تمایل به ارسال پیغام صوتی دکمه را نگه دارید و پیغام را ارسال کنید
    </div>
  );

  const hasPermission = (
    <span className="text-dark mt-4">
      {voiseUploaded ? (
        <VoiseRecorder
          setVoiseUploaded={setVoiseUploaded}
          setVoise={setVoise}
        />
      ) : (
        voiseTag
      )}
    </span>
  );

  return (
    <div className="row">
      <div className="col-6 text-start d-flex align-items-center mt-5 bg-white p-5">
        <div className="text-end text-md-start  col-12 col-md-11 order-4 order-md-4">
          <Link
            to="/"
            className="text-end text-md-end  col-12 col-md-11 order-4 order-md-4"
          >
            <button
              type="button"
              className="btn backgroundWhite text-dark text-center btn-edit position-relative"
            >
              درخواست ها<div className="dots-em"></div>
            </button>
          </Link>
        </div>
        <input type="radio" id="" name="" value="" hidden />
      </div>
      <div className="col-6 rtl text-start d-flex align-items-center mt-5 bg-white p-5">
        <button
          type="button"
          className="btn backgroundWhite text-dark text-center btn-edit position-relative "
        >
          درخواست جدید<div className="dots"></div>
        </button>
        <input type="radio" id="" name="" value="" hidden />
      </div>
      <div className="col-12 p-0" dir="rtl">
        <form onSubmit={sendNewRequest}>
          <div className="card rtl mt-3 border-none">
            <div className="card-body p-4 p-md-5">
              <h4 className="mb-4 text-right"> درخواست جدید </h4>

              <div className="row mb-4 p-0 mx-0">
                <div className="border-0 card card--inverted  order-0 order-md-0 mt-4 col-12 col-md-4">
                  <label className="input">
                    <select
                      onChange={secondDepartmentHandler}
                      className="input__field  input-placeholder formSelectLabel"
                      name="mainDepartment"
                      ref={mainDepartmentRef}
                      id="pet-select"
                    >
                      <option value="0">دسته بندی اول انتخاب کنید</option>
                      {mainDeparments.map((depatrment, index) => {
                        return (
                          <option
                            key={index}
                            className="form-control"
                            value={depatrment.id}
                          >
                            {depatrment.title}
                          </option>
                        );
                      })}
                    </select>
                  </label>
                </div>
                <div className="card card--inverted border-0 order-0 order-md-0 mt-4 col-12 col-md-4">
                  <label className="input">
                    <select
                      ref={secondDeparmentRef}
                      className="input__field input-placeholder formSelectLabel"
                      name="pets"
                      id="pet-select"
                    >
                      <option value="0">دسته بندی دوم انتخاب کنید</option>

                      {secondDeparments.map((secondDeparment, index) => {
                        return (
                          <option
                            key={index}
                            className="form-control"
                            value={secondDeparment.id}
                          >
                            {secondDeparment.title}
                          </option>
                        );
                      })}
                    </select>
                  </label>
                </div>
              </div>
              <div className="card card--inverted border-0 order-0 order-md-0">
                <label className="input">
                  <input
                    ref={subjectRef}
                    className="input__field form-control input-placeholder"
                    type="text"
                    placeholder="موضوع درخواست خود را وارد کنید"
                  />
                </label>
              </div>
              <div
                className="card card--inverted border-0 mt-5 order-1 order-md-1 text-right"
                dir="rtl"
              >
                <label className="input">
                  <textarea
                    ref={textRef}
                    className="input__field form-control pt-3"
                    rows="11"
                  ></textarea>
                  <span className="input__label">متن درخواست را وارد کنید</span>
                </label>
                {havePermissions ? hasPermission : null}
              </div>

              <FileUploader  token={token} setUploadedFiles={setUploadedFiles} uploadedFiles={uploadedFiles}/>
              
              <div className="row">
                <div className="text-center text-md-end mt-5 col-12 col-md-12 order-4 order-md-4 text-left">
                  <button
                    ref={submitRef}
                    type="submit"
                    className="btn backgroundGreen text-white text-center btn-edit"
                  >
                    ارسال درخواست
                    <img
                      alt=""
                      src={arrowLeft}
                      height="15"
                      className="mx-1 text-white"
                    />
                  </button>
                </div>
              </div>
            </div>
            <ToastContainer className="text-right" />
          </div>
        </form>
      </div>
    </div>
  );
}
export default NewRequest;
