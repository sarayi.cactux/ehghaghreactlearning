

import arrowDown from "../../../assests/image/icons/arrow-down.svg";
import { FromInt } from "../../common/NumberTools";
function Faq() {
  return (
    <div className="row">
      <div className="col-12">
        <div className="col-12 mt-5">
          <div id="accordion">
            <div className="accordion-card">
              <div
                className="accordion-card__header d-flex justify-content-around"
                id="headingOne"
                data-toggle="collapse"
                data-target="#collapseOne"
                aria-expanded="true"
                aria-controls="collapseOne"
              >
                <span>
                  <img src={arrowDown} width="16" height="16" alt="" />
                </span>
                <span className="mb-0 me-auto">
                  <span>سوال شما</span>
                </span>
              </div>
              <div
                id="collapseOne"
                className="collapse show"
                aria-labelledby="headingOne"
                data-parent="#accordion"
              >
                <div className="accordion-card__body text-start rtl">
                  {FromInt(`پاسخ پ۲وزن اسخ پاسخپاسخ 4434 sdf 345پاسخ پاسخپاسخپاسخ پاسخپاسخپاسخ رپاسخ پاسخ
                  پاسخپاسخ ر سوال`)}
                  
                </div>
              </div>
            </div>
            <div className="accordion-card">
              <div
                className="accordion-card__header d-flex justify-content-around"
                id="headingOne1"
                data-toggle="collapse"
                data-target="#collapseOne1"
                aria-expanded="false"
                aria-controls="collapseOne1"
              >
                <span>
                  <img src={arrowDown} width="16" height="16" alt="" />
                </span>
                <span className="mb-0 me-auto">
                  <span>سوال شما</span>
                </span>
              </div>
              <div
                id="collapseOne1"
                className="collapse "
                aria-labelledby="headingOne1"
                data-parent="#accordion"
              >
                <div className="accordion-card__body text-start rtl">
                  پاسخ پاسخ پاسخپاسخپاسخ پاسخپاسخپاسخ پاسخپاسخپاسخ رپاسخ پاسخ
                  پاسخپاسخ ر سوال
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Faq;
