import { Link } from "react-router-dom";
import React from "react";

import AuthApi from "../../store/AuthApi";

import RequestRow from "../../ui/Request";

function Home() {
  const Auth = React.useContext(AuthApi);
  const isLawyer = Auth.auth ? Auth.userInfo.isLawyer : false;
  const Consultings = Auth.auth ? Auth.userInfo.Consultings : [];
  const UserLanding = () => {
    return (
      <div className="row">
        <div className="col-6 text-start d-flex align-items-center mt-5 bg-white p-5">
          <div className="text-end text-md-start  col-12 col-md-11 order-4 order-md-4">
            <button
              type="button"
              className="btn backgroundWhite text-dark text-center btn-edit position-relative"
            >
              درخواست ها<div className="dots"></div>
            </button>
          </div>
          <input type="radio" id="" name="" value="" hidden />
        </div>
        <div className="col-6 rtl text-start d-flex align-items-center mt-5 bg-white p-5">
          <Link
            to="/newRequest"
            className="text-end text-md-end  col-12 col-md-11 order-4 order-md-4"
          >
            <button
              type="button"
              className="btn backgroundWhite text-dark text-center btn-edit position-relative "
            >
              درخواست جدید<div className="dots-em"></div>
            </button>
          </Link>
          <input type="radio" id="" name="" value="" hidden />
        </div>
        <div className="col-12 p-0" dir="rtl">
          <div className="card rtl mt-3 border-none">
            <div className="card-body p-4 p-md-5">
              <h4 className="mb-4 text-right"> درخواست ها </h4>

              <div className="table-responsive">
                <table className="table borderless">
                  <thead className="border-none border-btt borderless">
                    <tr className="text-center">
                      <th scope="col" className=" ">
                        شماره درخواست
                      </th>
                      <th scope="col" className=" mx-auto">
                        موضوع
                      </th>
                      <th scope="col" className="col-3  ">
                        وضعیت
                      </th>
                      <th scope="col" className=" mx-auto   ">
                        آخرین بروزرسانی
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {Consultings.map((item, index) => {
                      return <RequestRow item={item} key={index} />;
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };
  const LawyerLanding = () => {
    return <div>Lawyer</div>;
  };
  return(  <> {isLawyer ? LawyerLanding() : UserLanding()}</>  )
}
export default Home;
