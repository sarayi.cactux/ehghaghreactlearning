import { useParams, useNavigate, Link } from "react-router-dom";
import ReactAudioPlayer from "react-audio-player";

import React from "react";

import AuthApi from "../../store/AuthApi";
import { SuccessNotify, ErrorNotify } from "../../common/Toast";
import FileUploader from "../../layout/FileUploader";
import RequestDetailApi from "../../api/RequestDetail";
import NewMessage from "../../api/NewMessage";

import NewRequestApi from "../../api/NewRequest";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import VoiseRecorder from "../../layout/VoiseRecorder";

import microphone from "../../../assests/image/icons/microphone.svg";
import arrowLeft from "../../../assests/image/icons/arrow-left.svg";
import downloadIcon from "../../../assests/image/icons/download.svg";
import userMessagesIcon from "../../../assests/image/icons/user-messages.svg";
import payAdviceIcon from "../../../assests/image/icons/pay-advice.svg";

function RequestDetail() {
  const navigate = useNavigate();
  const { id } = useParams();

  const submitRef = React.useRef();
  const textRef = React.useRef();
  const Auth = React.useContext(AuthApi);
  const [consultingDetail, setConsultingDetail] = React.useState();
  const [uploadedFiles, setUploadedFiles] = React.useState([]);
  const [havePermissions, setHavePermissions] = React.useState(false);
  const [voiseUploaded, setVoiseUploaded] = React.useState(false);
  const [voise, setVoise] = React.useState(null);
  const [isDataRecived, setIsDataRecived] = React.useState(false);
  const [chooseAction, setChooseAction] = React.useState();
  const token = Auth.userInfo.token;
  React.useEffect(() => {
    Auth.setLoading(true);
    const findMainDepartment = async () => {
      const result = await RequestDetailApi(token, id);
      if (result.status === 403) {
        Auth.setUserInfo(null);
        Auth.setAuth(false);
        return;
      }
      if (result.status === 500) {
        ErrorNotify("Internal server error");
        return;
      }
      if (result.status === 400) {
        ErrorNotify(JSON.parse(result.response).message[0]);
        return;
      }
      if (result.status !== 200) {
        ErrorNotify(JSON.parse(result.response).error);
        return;
      }
      setConsultingDetail(result.data.consultingDetail);
      if(result.data.consultingDetail.statusCode === 4) setChooseAction(payHtml); 
      else if (result.data.consultingDetail.statusCode === 7) setChooseAction(ended); 
      else setChooseAction(messageForm)
       
      setIsDataRecived(true);
      Auth.setLoading(false);
    };
    findMainDepartment();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const isLawyer = false;
  if (isLawyer) {
    navigate("/");
  }

  const checkPermissions = async () => {
    const permissions = navigator.mediaDevices.getUserMedia({ audio: true });
    permissions
      .then((stream) => {
        setHavePermissions(true);
      })
      .catch((err) => {
        setHavePermissions(false);
      });
  };
  const sendNewMessage = async (event) => {
    event.preventDefault();

    const text = textRef.current.value;
    if (!text || text.length < 15) {
      ErrorNotify("متن پیام وارد نشده");
      return;
    }
    if (voiseUploaded) {
      uploadedFiles.push(voise);
    }

    const medias = uploadedFiles.map((item) => {
      return {
        mediaId: item.id,
        title: "",
      };
    });
    Auth.setLoading(true);
    const newRequestObj = {
      text,
      medias,
    };
    const newUserInfo = await NewRequestApi(token, newRequestObj);
    if (newUserInfo.status === 403) {
      Auth.setUserInfo(null);
      Auth.setAuth(false);
      return;
    }
    if (!newUserInfo) {
      Auth.setUserInfo(null);
      Auth.setAuth(false);
      return;
    }

    Auth.setUserInfo(newUserInfo.data);
    Auth.setLoading(false);

    SuccessNotify("پیام با موفقیت ثبت شد");
    setTimeout(() => {
      navigate("/");
    }, 3000);
  };
  checkPermissions();

  const voiseTag = (
    <div
      className="pointer"
      onClick={() => {
        setVoiseUploaded(true);
      }}
    >
      <img alt="" src={microphone} width="20" height="27" />
      در صورت تمایل به ارسال پیغام صوتی دکمه را نگه دارید و پیغام را ارسال کنید
    </div>
  );

  const hasPermission = (
    <span className="text-dark mt-4">
      {voiseUploaded ? (
        <VoiseRecorder
          setVoiseUploaded={setVoiseUploaded}
          setVoise={setVoise}
        />
      ) : (
        voiseTag
      )}
    </span>
  );
  const audioPlayer = (mediaUrl, i) => {
    return (
      <ReactAudioPlayer
        key={i}
        src={`${process.env.REACT_APP_HOST}files/${mediaUrl}`}
        controls={true}
        autoPlay={false}
      />
    );
  };
  const fileDownloader = (mediaUrl, i) => {
    return (
      <a
        key={i}
        className=" text-decoration-none"
        href={`${process.env.REACT_APP_HOST}files/${mediaUrl}`}
        target="_blank"
        download
        rel="noreferrer"
      >
        <span className="text-dark">
          <img alt="" src={downloadIcon} width="27" height="20" />
          دانلود فایل ضمیمه یک
        </span>
      </a>
    );
  };
  const userMessage = (message) => {
    return (
      <div key={message.id}>
        <div className=" card--inverted border-none mt-5 row">
          <div className="col-12 d-contents col-lg-1">
            <div className="massage-bg-radius d-flex align-items-center justify-content-center">
              <img src={userMessagesIcon} alt="" width="34" height="32" />
            </div>
          </div>
          <div className="col-12 col-lg-6 mt-3 mt-lg-0 ms-lg-4 ms-xxl-0 massage-bg p-3">
            <div className="row text-right text-justify" dir="rtl">
              <span className="text-dark"> کاربر </span>

              <p className="text-dark mt-2 font-p">{message.text}</p>
              {message.consultingMessageMedias.map((item, i) => {
                if (item.mediaObj.mimeType === 2) {
                  return audioPlayer(item.mediaObj.mediaUrl, i);
                }
              })}
              <div className="row my-3">
                {message.consultingMessageMedias.map((item, i) => {
                  if (item.mediaObj.mimeType !== 2) {
                    return fileDownloader(item.mediaObj.mediaUrl, i);
                  }
                })}
              </div>
            </div>
          </div>
        </div>
        <div className="col-12 text-center">
          <hr className="mt-5 mb-3" />
        </div>
      </div>
    );
  };
  const lawyerMessage = (message) => {
    return (
      <div key={message.id}>
        <div className="card--inverted border-none mt-4 row d-flex justify-content-end ">
          <div className="col-12 col-lg-6 mt-3 mt-lg-0 me-lg-4 me-xxl-0 massage-bg1 order-1 order-lg-0 p-3">
            <div className="row d">
              <span className="text-dark d-flex justify-content-end">
                کاربر
              </span>
              <p className="text-dark mt-2 font-p">{message.text}</p>
              {message.consultingMessageMedias.map((item, i) => {
                if (item.mediaObj.mimeType === "mp3") {
                  return audioPlayer(item.mediaObj.mediaUrl);
                }
              })}
              <div className="row my-3">
                {message.consultingMessageMedias.map((item, i) => {
                  if (item.mediaObj.mimeType !== "mp3") {
                    return fileDownloader(item.mediaObj.mediaUrl);
                  }
                })}
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-1 order-0 order-lg-1 d-contents justify-content-end justify-content-lg-start">
            <div className="massage-bg-radius d-flex align-items-center justify-content-center">
              <img src={userMessagesIcon} alt="" width="34" height="32" />
            </div>
          </div>
        </div>
        <div className="col-12 text-center">
          <hr className="mt-5 mb-3" />
        </div>
      </div>
    );
  };
  const payHtml = (
    <div className="col-12 mt-3 mt-lg-0 me-lg-4 me-xxl-0  order-1 order-lg-0  mt-4 text-left">
      <div className="row mt-4 ">
        <Link
          className="px-0 text-decoration-none d-flex justify-content-end"
          to="/files/myfile.pdf"
        >
          <span className="text-dark  p-2">هزار تومان 9000 </span>
          <span className="text-dark massage-bg p-2">
            <img src={payAdviceIcon} alt="" width="27" height="20" />
            پرداخت حق مشاور
          </span>
        </Link>
      </div>
    </div>
  );
  const messageForm = (
    <form onSubmit={sendNewMessage}>
      <h4 className="mb-4 text-right"> پیام جدید </h4>
      <div
        className="card card--inverted border-0 mt-5 order-1 order-md-1 text-right"
        dir="rtl"
      >
        <label className="input">
          <textarea
            ref={textRef}
            className="input__field form-control pt-3"
            rows="11"
          ></textarea>
          <span className="input__label">متن پیام را وارد کنید</span>
        </label>
        {havePermissions ? hasPermission : null}
      </div>

      <FileUploader
        token={token}
        setUploadedFiles={setUploadedFiles}
        uploadedFiles={uploadedFiles}
      />

      <div className="row">
        <div className="text-center text-md-end mt-5 col-12 col-md-12 order-4 order-md-4 text-left">
          <button
            ref={submitRef}
            type="submit"
            className="btn backgroundGreen text-white text-center btn-edit"
          >
            ارسال پیام
            <img
              alt=""
              src={arrowLeft}
              height="15"
              className="mx-1 text-white"
            />
          </button>
        </div>
      </div>
    </form>
  );
  const ended = <div><h2> درخواست پایان یافته است</h2></div>
  return (
    <div className="row">
      <div className="col-12 p-0" dir="rtl">
        <div className="card rtl mt-3 border-none">
          <div className="card-body p-4 p-md-5">
            <article className="l-design-widht row">
              {isDataRecived ? (
                <>
                  {consultingDetail.ConsultingMessages.map((message) => {
                    if (message.userId === Auth.userInfo.id) {
                      return userMessage(message);
                    } else {
                      return lawyerMessage(message);
                    }
                  })}
                </>
              ) : null}
            </article>
            { chooseAction }
           
          </div>
          <ToastContainer className="text-right" />
        </div>
      </div>
    </div>
  );
}

export default RequestDetail;
