import { Route, Routes } from "react-router-dom";
import React from "react";
import AuthApi from "./store/AuthApi";

import Page from "./layout/Page";
import Login from "./pages/Login";
import Redirect from "./common/Redirect";

import Faq from "./pages/user/faq";
import Home from "./pages/user/Home";
import NewRequest from "./pages/user/NewRequest";
import RequestDetail from "./pages/user/RequestDetail";

const AppRoutes = () => {
  const Auth = React.useContext(AuthApi);
  return (
    <Routes>
      <Route
        path="/"
        element={<ProtectAllRout auth={Auth.auth} component={Home} exact />}
      />
      <Route
        path="/newRequest"
        element={<ProtectAllRout auth={Auth.auth} component={NewRequest} />}
      />
      <Route
        path="/cd/:id"
        element={<ProtectAllRout auth={Auth.auth} component={RequestDetail} />}
      />
      <Route
        path="/faq"
        element={<ProtectAllRout auth={Auth.auth} component={Faq} />}
      />
      <Route path="/login" element={<ProtectLogin auth={Auth.auth} />} />

      <Route
        path="*"
        element={f404}
      />
    </Routes>
  );
};
const ProtectAllRout = (props) => {
  if (!props.auth) {
    return <Redirect route="/login" />;
  } else {
    return (
      <Page>
        <props.component />
      </Page>
    );
  }
};
const ProtectLogin = (props) => {
  if (!props.auth) {
    return <Login />;
  } else {
    return <Redirect />;
  }
};
const f404 = () => {
  return (
    <div>
      <h1>صفحه پیدا نشد</h1>
    </div>
  );
};
export default AppRoutes;
