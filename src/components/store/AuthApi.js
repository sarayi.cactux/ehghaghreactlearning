import { useState, createContext } from "react";

const AuthApi = createContext({
  auth: false,
  userInfo: null,
  loading: true,
  setLoading:(item)=>{},
  setAuth: (item) => {},
  setUserInfo: (item) => {},
});
export function AuthContextProvider(props) {
  const [auth, setAuthF] = useState(false);
  const [loading, setLoadingF] = useState(true);
  const [userInfo, setUserInfoHandler] = useState(null);

  const setUserInfo = (inf) => {
    setUserInfoHandler(inf);
  };
  const setAuth = (item) => {
    setAuthF(item);
  };
  const setLoading = (item) => {
    setLoadingF(item);
  };
  const context = {
    auth: auth,
    userInfo: userInfo,
    loading: loading,
    setAuth: setAuth,
    setLoading: setLoading,
    setUserInfo: setUserInfo,
  };
  return <AuthApi.Provider value={context}>{props.children}</AuthApi.Provider>;
}
export default AuthApi;
