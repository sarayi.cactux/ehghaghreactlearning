import React from "react";
import AudioReactRecorder, { RecordState } from 'audio-react-recorder';
import ReactAudioPlayer from 'react-audio-player';
import AuthApi from "../store/AuthApi";
import UploadFile from "../api/UploadFile";
import DeleteFile from "../api/DeleteFile";

import deleteIcon from "../../assests/image/icons/delete.svg";
import { FromInt } from "../common/NumberTools";

import microphone from "../../assests/image/icons/microphone.svg";
import pauseIcon from "../../assests/image/icons/pause.svg";
import stopIcon from "../../assests/image/icons/stop.svg";




function VoiseRecorder(props) {
  const Auth = React.useContext(AuthApi);
  const token = Auth.userInfo.token;
  const [recordState, setRecordState] = React.useState(null);
  const [timer, setTimer] = React.useState(0);
  const [recording, setRecording] = React.useState(false);
  const [uploadedViose, setUploadedViose] = React.useState(null);

  React.useEffect(() => {
    start()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
 

  const timerHandler = ()=>{
   
   const id = setTimeout(()=>{
          setTimer((prevTimer) => {
            return prevTimer+=1;
          });
          timerHandler();
        },1000);
        setRecording(id)  
    
  }
  const start = () => {
    setRecording(true);
    timerHandler()
    setRecordState(RecordState.START);
  }
  const stop = () => {
    setRecording(false);
    clearTimeout(recording);
    setRecordState(RecordState.STOP);
    
  }
  const pause = () => {
    setRecording(false);
    clearTimeout(recording);
    setRecordState(RecordState.PAUSE);
  }
  const onStop = async(audioData) => {
    Auth.setLoading(true);
    const audioBlob = await fetch(audioData.url).then(r => r.blob());
    const audiofile = new File([audioBlob], "audiofile.mp3", { type: "audio/mp3" })
    const formData = new FormData();
    formData.append("file", audiofile);
    const response = await UploadFile(token, formData);
    setUploadedViose(response.data);
    props.setVoise(response.data);
    props.setVoiseUploaded(true);
    Auth.setLoading(false);
  }
  const deleteUploadedVoise = async() =>{
    DeleteFile(token, uploadedViose.id)
    setUploadedViose(null);
    props.setVoise(null);
    props.setVoiseUploaded(false);
  }
  const resumeBtn = <img width="25px" alt="Resume" src={microphone} onClick={start}/>
  const pauseBtn = <img width="25px" alt="pause" src={pauseIcon} onClick={pause}/>
  const stopBtn = <img width="25px" alt="stop" src={stopIcon} onClick={stop}/>

  const updoaded= ()=>{
    return (
      <div className="row text-right">
        <div className="col-10 mt-1 mb-1 col-md-11  my-auto pr-0 mr-0">
        <ReactAudioPlayer
  src={`${process.env.REACT_APP_HOST}files/${uploadedViose.mediaUrl}`}
  controls={true}
  autoPlay={true}
/>
        </div>
        <div className="col-2 col-md-1 text-left  my-auto">
          <img
            alt=""
            src={deleteIcon}
            height="25"
            className="mx-1 text-white pointer"
            onClick={() => {
              deleteUploadedVoise()
            }}
          />
        </div>
        <hr></hr>
      </div>
    );
  }
  const notUploaded = ()=>{
    return( <div className="row">
      <div className="col-md-4 text-center "><AudioReactRecorder type="mp3" canvasWidth="300" backgroundColor="#fff" foregroundColor="#919191"  canvasHeight="35" state={recordState} onStop={onStop} /></div>
      <div className="col-4 text-right col-md-1 pr-5">{FromInt(timer.toString())}</div>
      <div className="col-4 text-center col-md-1">{recording? pauseBtn: resumeBtn}</div>
      <div className="col-4 text-left col-md-1 pl-5"> {stopBtn}</div>
     
    
      
    </div>);
  }

  return(<div> {uploadedViose? updoaded(): notUploaded()}</div>);
}

export default VoiseRecorder;
