import React from "react";
import $ from "jquery";
import AuthApi from "../store/AuthApi";

const Loading = () => {
  const Auth = React.useContext(AuthApi);
  if (Auth.loading) {
    $("#mainContainer").addClass("blur");
    $("#loading").removeClass("d-none");
  } else {
    setTimeout(() => {
       $("#mainContainer").removeClass("blur");
       $("#loading").addClass("d-none");
    }, 500);
  }
};
export default Loading;
