import Layout from "./Layout";

const Page = (props) => {
  return <Layout>{props.children}</Layout>;
};
export default Page;
