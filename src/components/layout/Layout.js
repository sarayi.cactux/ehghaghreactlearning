import { useState } from "react";

import TopNav from "./TopNav";
import $ from "jquery";

function Layout(props) {
  const [sideBar, setSiteBar] = useState(true);

  const openNav = () => {
    setSiteBar(false);
    $("#mySidebar").css("right", 0);
  };

  const closeNav = () => {
    $("#mySidebar").css("right", "-250px");
  };
  const closeSideBar = () => {
    setSiteBar(true);
  };
  return (
    <div>
      <TopNav openNav={openNav} closeNav={closeNav} />
      {sideBar ? closeNav() : null}
      <div className="container" id="mainContainer" onClick={closeSideBar}>
        {props.children}
      </div>
    </div>
  );
}

export default Layout;
