import { Link } from "react-router-dom";
import React from "react";
import Cookies from "js-cookie";

import AuthApi from "../store/AuthApi";
import userIcon from "../../assests/image/icons/user.svg";
import profileIcon from "../../assests/image/icons/profile.svg";
import transaction_list from "../../assests/image/icons/transaction_list.svg";
import transactions from "../../assests/image/icons/transactions.svg";
import online_support from "../../assests/image/icons/online_support.svg";
import faqIcon from "../../assests/image/icons/faq.svg";
import backIcon from "../../assests/image/icons/back.svg";
import { FromInt } from "../common/NumberTools";

import LogOut from "../api/LogOut";

function TopNav(props) {
  const Auth = React.useContext(AuthApi);
  const mobile = Auth.auth ? FromInt(Auth.userInfo.mobile) : "";
  const isLawyer = Auth.auth ? Auth.userInfo.isLawyer : false;
  const logOut = () => {
    const token = Cookies.get("ehghaghUser");
    LogOut(token);
    Cookies.remove("ehghaghUser");
    Auth.setAuth(false);
  };
  const UserNav = () => {
    return (
      <div className="p-2 m-3 text-start" onClick={props.closeNav}>
        <div>
          <Link to="/">
            درخواست ها
            <img
              alt=""
              src={transaction_list}
              style={{ color: "white" }}
              width="23"
            />
          </Link>
        </div>
        <Link to="">
          تراکنش ها
          <img
            alt=""
            src={transactions}
            style={{ color: "white" }}
            width="23"
          />
        </Link>
        <Link to="/faq">
          پرسشهای متداول
          <img alt="" src={faqIcon} style={{ color: "white" }} width="23" />
        </Link>
        <Link to="/ticket">
          پشتیبانی
          <img
            alt=""
            src={online_support}
            style={{ color: "white" }}
            width="23"
          />
        </Link>
        <Link to="/" onClick={logOut}>
          خروج
          <img
            alt=""
            src={backIcon}
            style={{ color: "white" }}
            width="23"
          />
        </Link>
      </div>
    );
  };
  const LawyerNav = () => {
    return (
      <>
        <div
          onClick={props.closeNav}
          className="p-2  m-3 rounded text-start "
          style={{ backgroundColor: "#F8F8F8" }}
        >
          <Link to="" href="">
            پروفایل
            <img
              className=""
              alt=""
              src={profileIcon}
              style={{ color: "white" }}
              width="23"
            />
          </Link>
          <ul className="profile-menu" dir="rtl">
            <li>
              <Link to="/" className="visit-profile">
                مشاهده پروفایل
              </Link>
            </li>
            <li>
              <Link to="/" className="edit-profile">
                ویرایش پروفایل
              </Link>
            </li>
          </ul>
        </div>

        <div className="p-2 m-3 text-start" onClick={props.closeNav}>
          <div>
            <Link to="">
              تسویه حساب
              <img
                alt=""
                src={transaction_list}
                style={{ color: "white" }}
                width="23"
              />
            </Link>
          </div>
          <Link to="">
            تراکنش
            <img
              alt=""
              src={transactions}
              style={{ color: "white" }}
              width="23"
            />
          </Link>
          <Link to="">
            پشتیبانی
            <img
              alt=""
              src={online_support}
              style={{ color: "white" }}
              width="23"
            />
          </Link>
          <Link to="/faq">
            پرسشهای متداول
            <img alt="" src={faqIcon} style={{ color: "white" }} width="23" />
          </Link>
        </div>
      </>
    );
  };
  return (
    <nav className="navbar navbar-expand-lg header-site  pr-0 pl-0">
      <div className="container-fluid p-0">
        <div id="mySidebar" className="sidebar text-start mySidebar">
          <span to="" onClick={props.closeNav} className="closebtn">
            ×
          </span>

          <div className="p-2 border m-3 rounded text-center">
            <span className="welcome-text"> به احقاق خوش آمدید </span>
          </div>

          {isLawyer ? LawyerNav() : UserNav()}
        </div>

        <div className=" col-6 row">
          <div className="col-auto exit">
            <Link to="" onClick={logOut} className="a-menu exit">
              {" "}
              خروج{" "}
            </Link>
          </div>
          <div className="col-auto">
            <Link to="" className="a-menu fa-num">
              {mobile}
            </Link>
            <img alt="" src={userIcon} style={{ color: "white" }} width="23" />
          </div>
        </div>
        <div className="navbar-nav me-auto  ">
          <div
            id="main rtl"
            style={{ cursor: "pointer" }}
            onClick={props.openNav}
          >
            <span className="a-menu">فهرست</span>
            <button className="openbtn">☰</button>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default TopNav;
