import React from "react";

import UploadFile from "../api/UploadFile";
import DeleteFile from "../api/DeleteFile";

import deleteIcon from "../../assests/image/icons/delete.svg";


const FileUploader = (props) => {
  const uploadInputRef = React.useRef();
  const [selectedFile, setSelectedFile] = React.useState(null);
  const handleUpload = async () => {
    if (!selectedFile) return;
    const formData = new FormData();
    formData.append("file", selectedFile);
    try {
      const response = await UploadFile(props.token, formData);
      addUploadedFiles(response.data);
      setSelectedFile(null);
      uploadInputRef.current.value = null;
    } catch (error) {
      console.log(error);
    }
  };

  const handleFileSelect = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  const removeUploadedFiles = (itemId) => {
    DeleteFile(props.token, itemId);
    props.setUploadedFiles((prevUploadedFiles) => {
      return prevUploadedFiles.filter((item) => item.id !== itemId);
    });
  };

  const addUploadedFiles = (item) => {
    props.setUploadedFiles((prevUploadedFiles) => {
      return prevUploadedFiles.concat(item);
    });
  };
  return (
    <div>
      <div className="row text-right mt-5">
        <h4>افزودن پیوست</h4>
        <div className="card card--inverted border-0 mt-5 col-12 col-md-5 order-2 order-md-2 ">
          <label className="input ">
            <input
              className="input__field form-control attachmentInput "
              id="file"
              type="file"
              ref={uploadInputRef}
              onChange={handleFileSelect}
            />
          </label>
        </div>
        <div className="pointer  border-none mt-5  col-12 col-md-2 order-3 order-md-3">
          <div className="attachment d-flex align-items-center justify-content-center">
            <span className="" onClick={handleUpload}>
              ارسال پیوست
            </span>
          </div>
        </div>
      </div>
      <div className="col-12 text-right mt-5" dir="rtl">
        {props.uploadedFiles.map((uploadedFile, index) => {
          return (
            <div className="row text-right" key={index}>
              <div className="col-11 mt-1 mb-1 col-md-11">
                {uploadedFile.mediaUrl}
              </div>
              <div className="col-1 col-md-1">
                <img
                  alt=""
                  src={deleteIcon}
                  height="25"
                  className="mx-1 text-white pointer"
                  onClick={() => {
                    removeUploadedFiles(uploadedFile.id);
                  }}
                />
              </div>
              <hr></hr>
            </div>
          );
        })}
      </div>
    </div>
  );
};
export default FileUploader;
