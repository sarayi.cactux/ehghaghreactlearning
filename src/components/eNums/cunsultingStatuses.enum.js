const cunsultingStatuses = {
  newRequest: {
    code: 1,
    style: "btn-cheaking",
    text: 'در انتظار بررسی',
  },
  pending: {
    code: 2,
    style: "btn-cheaking",
    text: 'در حال بررسی',
  },
  answered: {
    code: 3,
    style: "btn-confirm",
    text: 'پاسخ داده شده',
  },
  awaitingPayment: {
    code: 4,
    style: "btn-reject",
    text: 'در انتظار پرداخت',
  },
  paid: {
    code: 5,
    style: "btn-confirm",
    text: 'پرداخت شده',
  },
  customerAnswer: {
    code: 6,
    style: "btn-confirm",
    text: 'پاسخ مشتری',
  },
  closed: {
    code: 7,
    style: "btn-reject",
    text: 'بسته شده',
  },
};
export default cunsultingStatuses;
