import { Link } from "react-router-dom";

import cunsultingStatuses from "../eNums/cunsultingStatuses.enum";
import { FromInt } from "../common/NumberTools";

const RequestRow = (props) => {
  const { item } = props;
  const code = `#${item.id}`;
  const subject = item.subject;
  const status = item.status;
  const updatedAt = FromInt(item.updatedAt);
  const statuscode = Object.keys(cunsultingStatuses).find(
    (e) => cunsultingStatuses[e].code === item.statusCode
  );
  return (
    <tr className="text-center border-bottom">
      <td className="">
        <p className="my-3"> {code} </p>
      </td>

      <td className="  mx-auto">
        <p className="my-3">{subject}</p>
      </td>
      <td className="">
        <Link to={`/cd/${item.id}`}>
          <button
            className={`btn btn-allert ${cunsultingStatuses[statuscode].style}`}
          >
            {status}
          </button>
        </Link>
      </td>

      <td className="">
        <p className="my-3">{updatedAt}</p>
      </td>
    </tr>
  );
};

export default RequestRow;
