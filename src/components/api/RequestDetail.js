import axios from "axios";

const RequestDetail = async (token, id) => {
  let config = {
    headers: {
      ehghagh_jwt: token,
    },
  };
  try {
    const result = await axios.get(
      `${process.env.REACT_APP_HOST}usersConsulting/${id}`,
      config
    );

    return result;
  } catch (error) {
    return error.request;
  }
};

export default RequestDetail;
