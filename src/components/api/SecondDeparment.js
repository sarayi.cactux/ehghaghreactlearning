import axios from "axios";


const SecondDeparment = async (mainDepartmentId) => {

  try {
  const result = await axios.get(`${process.env.REACT_APP_HOST}admin/basicDefinitions/${mainDepartmentId}`);
  
  return result;
} catch (error) {
  return false;
}
};

export default SecondDeparment;
