import axios from "axios";

const SingIn = async (mobile) => {
  const result = await axios.post(`${process.env.REACT_APP_HOST}users/signIn`, {
    mobile,
  });
  return result;
};

export default SingIn;
