import axios from "axios";

const UploadFile = async (token, formData) => {
  let config = {
    headers: {
      ehghagh_jwt: token,
      "Content-Type": "multipart/form-data",
    },
  };

  try {
    const result = await axios.post(
      `${process.env.REACT_APP_HOST}users/uploadFile`,
      formData,
      config,
    );
    return result;
  } catch (error) {
    return false;
  }
};

export default UploadFile;
