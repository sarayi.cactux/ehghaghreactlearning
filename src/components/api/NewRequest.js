import axios from "axios";


const NewRequest = async (token, newRequestObj) => {
  let config = {
    headers: {
      ehghagh_jwt: token,
    },
  };


  try {
  const result = await axios.post(`${process.env.REACT_APP_HOST}usersConsulting`, newRequestObj, config);
  return result;
} catch (error) {
  return false;
}
};

export default NewRequest;
