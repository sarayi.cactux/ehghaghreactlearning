import axios from "axios";

const LogOut = async (token) => {
  let config = {
    headers: {
      ehghagh_jwt: token,
    },
  };
  try {
  const result = await axios.get(`${process.env.REACT_APP_HOST}users/signOut`,  config);
  return result;
} catch (error) {
  return false;
}
};


export default LogOut;
