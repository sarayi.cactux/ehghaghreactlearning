import axios from "axios";

const LoginUser = async (token, code) => {
  const config = {
    headers: {
      singInToken: token,
    },
  };

  const data = {
    code,
  };
  try {
  const result = await axios.put(`${process.env.REACT_APP_HOST}users/signIn`, data, config);
  return result;
} catch (error) {
  return false;
}
};

export default LoginUser;
