import axios from "axios";


const DeleteFile = async (token, id) => {
  const config = {
    headers: {
      ehghagh_jwt: token,
    },
  };
  try {
  const result = await axios.delete(`${process.env.REACT_APP_HOST}users/${id}`,  config);
  
  return result;
} catch (error) {
  return false;
}
};

export default DeleteFile;
