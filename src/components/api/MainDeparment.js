import axios from "axios";


const MainDepartment = async () => {

  try {
  const result = await axios.get(`${process.env.REACT_APP_HOST}admin/basicDefinitions`);
  
  return result;
} catch (error) {
  return false;
}
};

export default MainDepartment;
