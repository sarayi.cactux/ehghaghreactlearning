import axios from "axios";


const GetUserInf = async (token) => {
  let config = {
    headers: {
      ehghagh_jwt: token,
    },
  };
  try {
  const result = await axios.get(`${process.env.REACT_APP_HOST}users`,  config);
  
  return result;
} catch (error) {
  return false;
}
};

export default GetUserInf;
