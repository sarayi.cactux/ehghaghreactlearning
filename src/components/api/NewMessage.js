import axios from "axios";

const NewMessage = async (token, id, text, medias) => {
  const config = {
    headers: {
      ehghagh_jwt: token,
    },
  };
  const data = {
    text,
    medias,
  };

  try {
    const result = await axios.post(
      `${process.env.REACT_APP_HOST}usersConsulting/${id}}`,
      data,
      config
    );
    return result;
  } catch (error) {
    return false;
  }
};

export default NewMessage;
