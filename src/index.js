import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";

import "./assests";
import { AuthContextProvider } from "./components/store/AuthApi";
import App from "./App.js";
import * as serviceWorkerRegistration from "./serviceWorkerRegistration";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
 
  <AuthContextProvider>
      <BrowserRouter>
        <> <App /> </>
      </BrowserRouter>
  </AuthContextProvider>
 
);
serviceWorkerRegistration.register();
